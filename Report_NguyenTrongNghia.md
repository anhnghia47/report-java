# **Docker & Virtual Machine**
## **1. Docker**
Docker là một nền tảng để cung cấp cách để building, deploying và running ứng dụng dễ dàng hơn bằng cách sử dụng các containers (trên nền tảng ảo hóa). Ban đầu viết bằng Python, hiện tại đã chuyển sang Golang.

## **2. So sánh Container và Virtual Machine**

![](image/docker_vm.png)

### **2.1 Tài nguyên (resource)**

[VM] Mọi thứ đều bị giới hạn bởi phần cứng ảo.

[C] Process trong container sử dụng trực tiếp tài nguyên thật, nhưng HĐH có thể quy định mỗi process một mức giới hạn tài nguyên khác nhau (hoặc không giới hạn).

### **2.2 Thực thi (execution)**

[VM] HĐH thật → HĐH ảo → HĐH ảo chạy phần mềm.

[C] HĐH thật chạy phần mềm.

### **2.3 Sự tối ưu (performance)**

[VM] Phần cứng thật phải gánh cả một HĐH ảo. Từ khi máy tính khởi động lên, cho tới khi sử dụng được phần mềm rất mất thời gian.

[C] Phần mềm thật chạy trên phần cứng thật. Tốc độ khởi động gần như một phần mềm bình thường.


### **2.4 Tính bảo mật (Security)**

[VM] Phần mềm có mã độc có thể ảnh hưởng tới tài nguyên của process khác trong cùng VM.

[C] Process trong cùng container vẫn có thể ảnh hưởng tới nhau. Nhưng thông thường mỗi container chỉ nên chạy một process. Process khác container không thể gây ảnh hưởng cho nhau.

[VM] và [C]: Process trong môi trường ảo không thể truy xuất tới môi trường của HĐH chủ.

**Nói chung, container có kích cỡ nhẹ hơn và mang tính “di động” hơn so với VM.** 

---

# **Repository Pattern giao tiếp với DB**
## **Repository Pattern**

Là một mẫu để tạo ra 1 lớp abstraction trung gian giữa lớp data và lớp business. Lớp này chứa đựng phương thức thao tác giao tiếp với lớp data để phục vụ cho business từ lớp logic . Mục đích tạo ra lớp này để cách ly với việc tiếp cận data sao cho những thay đổi không ảnh hưởng trực tiếp đến lớp logic business.

Ví dụ: Khách hàng người muốn dùng Microsoft SQL, người khác thì muốn dùng Oracle hoặc DB khác. Nhiệm vụ Repo là phân tách các lớp và thêm một lớp tiếp cần data mới

---

# **REST & SOAP**
## **1. REST và SOAP**
**REST** là viết tắt của REpresentational State Transfer (Chuyển giao trạng thái phản hồi) trong khi **SOAP** là Simple Object Access Protocol (Giao thức truy cập đối tượng đơn giản).

## **2. Sự khác nhau giữa REST & SOAP**
### **2.1 Kiến trúc và giao thức**

- **REST** là một kiểu kiến trúc, từ đó RESTful web service được xây dựng trong khi **SOAP** là một chuẩn được tạo ra để chuẩn hóa giao tiếp giữa client và server về format, structure và method.

- **REST** có tất cả những lợi ích của giao thức HTTP, bao gồm GET, POST, PUT và DELETE. Trong khi đó **SOAP** sử dụng các message dạng XML để giao tiếp với server.

### **2.2 Các dạng format được support**
- RESTful web service có thể trả về data dưới dạng JSON, XML hoặc HTML, trong khi SOAP web service thì chỉ có thể sử dụng XML bởi vì các quy tắc trong một message SOAP đã được định nghĩa sẵn trong định dang XML.

### **2.3 Tốc độ**
- RESTful nhanh hơn so với SOAP vì các message của SOAP cần parsing nhiều hơn RESTful.
  
### **2.4 Cơ chế vận chuyển**
- Vì các message của SOAP được đóng gói định dạng đặc biệt nên có thể gửi qua bất kỳ cơ chế vận chuyển nào như TCP, FTP, SMTP,… 
- Mặt khác thì RESTful phụ thuộc rất nhiều vào giao thức HTTP. Tuy nhiên trong thực tế cũng sử dụng giao thức HTTP nhiều hơn nên lợi thế này của SOAP cũng không mấy được coi trọng.
  
### **2.5 Xác định tài nguyên truy cập**
- RESTful sử dụng URL để xác định tài nguyên mong muốn được truy cập trong khi SOAP sử dụng các message dạng XML để xác định các thủ tục hay tài nguyên yêu cầu.

**Tóm lại, RESTfull đơn giản, linh hoạt và dễ chịu hơn nhiều so với SOAP trong Java.**

---
# **Nguyên tắc thiết kế REST chuẩn**

- Hầu hết bây giờ đều chọn JSON là format chính nhưng có thể dùng 1 số format khác như XML 
- Nên sử dụng snake_case hoặc camelCase
- Chỉ sử dụng danh từ số nhiều để giữ tính nhất quán cho URL
- Không sử dụng động từ: ví dụ GET /getAllProducts
- Nhất quán trong việc đặt tên:
    - Sử dụng '/' để chỉ ra mối quan hệ thứ bậc
    - Sử dụng '-' để đọc các tên đường dẫn dài
    - Không dùng  '_'
    - Sử dụng chữ thường trong URL
- Liên kết trong resource: Mỗi resource chỉ nên liên kết tối đa 2 đối tượng, tránh rối loạn
- Xây dựng bộ search/filtering
- Version
    - Là 1 điều bắt buộc với các resource
    - Đặt ở vị trí đầu tiên của resource. VD: v1.5/users/1
- Phân trang
    - Truyền vào 2 tham số: OFFSET, LIMIT
    - OFFSET: chỉ định bắt đầu từ hàng nào sẽ bắt đầu truy xuất dữ liệu
    - LIMIT: Giới hạn số lượng bản ghi trong 1 trang
- Tìm kiếm: ví dụ GET /search?id=1
- Sắp xếp fields: ví dụ GET /cars?sort=-manufactorer,+model
- Xử lí, phân loại lỗi - Error Handling: Trả về trạng thái lỗi và message tương ứng
- Sử dụng SSL/TLS: Mã hóa thông tin gửi đi và trả về của API

Và rất nhiều rule nữa.

---
# **Spring**
## 1.Spring là gì?

![](image/spring_struc.png)

- Core Container: Đây là phần quan trọng nhất và cũng là phần cơ bản, nền tảng của Spring.
- AOP: module này hỗ trợ cài đặt lập trình hướng khía cạnh (Aspect Oriented Programming). Hiểu nôm na là kiểu lập trình cho phép chúng ta có thể thêm những đoạn code xử lý mới vào các ứng dụng đã tồn tại mà không cần phải chỉnh sửa code của các ứng dụng đó.
- DAO: (Data Access Object) là một trong patterm đảm bảo cho việc thay đổi DB không bị ảnh hưởng
- ORM: module này cung cấp khả năng giao tiếp với database. Làm việc với DB thông qua các Object
- JEE: (Em đọc nhưng chưa hiểu lắm!)
- Web: Hay còn gọi là Spring MVC. Nó hỗ trợ việc tạo ứng dụng web.
